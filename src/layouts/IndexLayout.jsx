import { Outlet } from "react-router-dom"

export default function IndexLayout() {
    return (
        <main className="w-full bg-gray-200 min-h-screen" >
            <div className="w-full h-14 bg-white sticky top-0 flex items-center z-20" >
                <div className="container mx-auto flex justify-between items-center ">
                    <h3 className="text-xl font-bold">Inmuebles app</h3>
                </div>
            </div>
            <Outlet />
        </main>
    )
}
