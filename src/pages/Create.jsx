import { useParams, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import apiInmuebles from "../controllers/inmueblesController";

export default function Create() {
    const { id } = useParams();
    const navigate = useNavigate();

    const [formdata, setFormdata] = useState({
        nombre: '',
        direccion: '',
        telefono: '',
        capacidadAforo: ''
    })
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);


    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormdata((prevData) => ({
            ...prevData,
            [name]: value
        }));
    };


    const handleSubmit = async (e) => {
        e.preventDefault();
        setLoading(true);
        try {
            if (id) await apiInmuebles.updateInmueble(id, {...formdata, id});
            else await apiInmuebles.createInmueble(formdata);
            navigate("/")
        } catch (err) {
            setError(err);
        }
        setLoading(false);
    };

    const fetchDetailsId = async () => {
        try {
            const payload = await apiInmuebles.getInmuebleById(id)
            delete payload.id
            setFormdata(payload)
        }
        catch (err) {
            console.log("No se ha podido obtener la información del inmueble.", err.message)
        }
    }

    useEffect(() => { if (id) fetchDetailsId(); }, [id])

    return (
        <section className='max-w-lg mx-auto mt-5 bg-white rounded-xl p-8 shadow'>
            <div className="divide-y-2" >
                <h2 className='text-xl '>
                    {
                        id ? "Editar detalles del Inmueble" : "Crear inmueble"
                    }

                </h2>
                <form onSubmit={handleSubmit} className="flex flex-col gap-4">
                    <div className="flex flex-col gap-5 mt-3 py-5">
                        <div className="flex flex-col" >
                            <label>Nombre:</label>
                            <input
                                className="py-0.5 px-3 border border-gray-600 bg-gray-100 rounded-full ml-2 flex-1"
                                type="text"
                                name="nombre"
                                value={formdata.nombre}
                                onChange={handleChange}
                                required
                            />
                        </div>
                        <div className="flex flex-col" >
                            <label>Dirección:</label>
                            <input
                                className="py-0.5 px-3 border border-gray-600 bg-gray-100 rounded-full ml-2 flex-1"
                                type="text"
                                name="direccion"
                                value={formdata.direccion}
                                onChange={handleChange}
                                required
                            />
                        </div>
                        <div className="flex flex-col" >
                            <label>Teléfono:</label>
                            <input
                                className="py-0.5 px-3 border border-gray-600 bg-gray-100 rounded-full ml-2 flex-1"
                                type="text"
                                name="telefono"
                                value={formdata.telefono}
                                onChange={handleChange}
                                required
                            />
                        </div>
                        <div className="flex flex-col" >
                            <label>Capacidad de Aforo:</label>
                            <input
                                className="py-0.5 px-3 border border-gray-600 bg-gray-100 rounded-full ml-2 flex-1"
                                type="number"
                                name="capacidadAforo"
                                value={formdata.capacidadAforo}
                                onChange={handleChange}
                                required
                            />
                        </div>
                        {error && <p>Error guardando el inmueble: {error.message}</p>}
                    </div>
                    <div className="flex justify-end gap-4 py-5">
                        {
                            loading
                                ? <h4>Guardando...</h4>
                                : <>
                                    <button
                                        type="submit"
                                        className='bg-blue-700 text-white px-5 py-2 rounded hover:bg-blue-600'
                                    >
                                        Guardar
                                    </button>
                                    <button
                                        onClick={() => navigate(-1)}
                                        className='bg-red-500 text-white px-5 py-2 rounded hover:bg-red-400'
                                    >
                                        Cancelar
                                    </button>
                                </>
                        }
                    </div>
                </form>
            </div>

        </section>
    )
}
