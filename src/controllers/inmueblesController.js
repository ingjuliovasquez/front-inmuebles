import axios from 'axios';

// URL principal del backend
const API_URL = 'https://localhost:7111/api/Inmuebles';

const apiInmuebles = {
    // Obtener la lista de todos los inmuebles
    getAllInmuebles: async () => {
        try {
            const response = await axios.get(API_URL);
            return response.data;
        } catch (error) {
            console.error('Error fetching inmuebles:', error);
            throw error;
        }
    },

    // Obtener un inmueble por ID
    getInmuebleById: async (id) => {
        try {
            const response = await axios.get(`${API_URL}/${id}`);
            return response.data;
        } catch (error) {
            console.error(`Error fetching inmueble with id ${id}:`, error);
            throw error;
        }
    },

    // Crear un nuevo inmueble
    createInmueble: async (payload) => {
        try {
            const response = await axios.post(API_URL, payload);
            return response.data;
        } catch (error) {
            console.error('Error creating inmueble:', error);
            throw error;
        }
    },

    // Actualizar un inmueble existente
    updateInmueble: async (id, payload) => {
        try {
            const response = await axios.put(`${API_URL}/${id}`, payload);
            return response.data;
        } catch (error) {
            console.error(`Error updating inmueble with id ${id}:`, error);
            throw error;
        }
    },

    // Eliminar un inmueble existente
    deleteInmueble: async (id) => {
        try {
            await axios.delete(`${API_URL}/${id}`);
        } catch (error) {
            console.error(`Error deleting inmueble with id ${id}:`, error);
            throw error;
        }
    }
};

export default apiInmuebles;
