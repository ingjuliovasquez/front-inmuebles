import { useState, useEffect } from 'react';
import apiInmuebles from '../controllers/inmueblesController';
import { Link } from 'react-router-dom';

export default function Index() {
    const [inmuebles, setInmuebles] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);

    const fetchInmuebles = async () => {
        try {
            const data = await apiInmuebles.getAllInmuebles();
            setInmuebles(data);
            setLoading(false);
        } catch (err) {
            setError(err);
            setLoading(false);
        }
    };

    useEffect(() => { fetchInmuebles() }, []);


    const deleteInmueble = async (id) => {
        try {
            setLoading(true)
            await apiInmuebles.deleteInmueble(id);
            await fetchInmuebles()
            setLoading(false)
        }
        catch (err) {
            console.log(err)
            setLoading(false)
        }
    }



    if (loading) return <section className="container mx-auto"><p>Loading...</p></section>;
    if (error) return <section className="container mx-auto"><p>Error loading inmuebles: {error.message}</p></section>;
    return (
        <section className="container mx-auto w-full mt-5" >
            <div className="w-full flex justify-between" >
                <h4 className="text-xl" >
                    Lista de inmuebles
                </h4>
                <Link to="/create" className="bg-blue-700 text-white px-5 py-2 rounded hover:bg-blue-600" >
                    Agregar
                </Link>
            </div>
            <ul className="divide-gray-200 max-w-xl mx-auto rounded gap-2">
                {inmuebles.map(inmueble => (
                    <li key={inmueble.id} className="py-1 sm:py-2">
                        <div className="flex items-center bg-white p-5 rounded-xl">
                            <div className="flex-1 min-w-0 ms-4">
                                <p className="text-sm font-medium text-gray-900 truncate ">
                                    {inmueble.nombre}
                                </p>
                            </div>
                            <div className="flex items-center text-base text-gray-900 gap-2">
                                <Link
                                    to={`/details/${inmueble.id}`}
                                    className='bg-blue-700 text-white px-5 py-2 rounded hover:bg-blue-600'
                                >
                                    Detalles
                                </Link>
                                <button onClick={() => deleteInmueble(inmueble.id)}
                                    className='bg-red-500 text-white px-5 py-2 rounded hover:bg-red-400'
                                >
                                    Eliminar
                                </button>
                            </div>
                        </div>
                    </li>
                ))}
            </ul>
        </section>
    )
}
