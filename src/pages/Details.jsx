import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import apiInmuebles from '../controllers/inmueblesController';
import { Link } from 'react-router-dom';

const Details = () => {
    const { id } = useParams();
    const [inmueble, setInmueble] = useState(null);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);

    useEffect(() => {
        const fetchInmueble = async () => {
            try {
                const data = await apiInmuebles.getInmuebleById(id);
                setInmueble(data);
                setLoading(false);
            } catch (err) {
                setError(err);
                setLoading(false);
            }
        };

        fetchInmueble();
    }, [id]);

    if (loading) return <section className="container mx-auto"><p>Loading...</p></section>;
    if (error) return <section className="container mx-auto"><p>Error loading inmueble: {error.message}</p></section>;
    if (!inmueble) return <section className="container mx-auto"><p>No se enncontró el inmueble especificado</p></section>;

    return (
        <section className='max-w-lg mx-auto mt-5 bg-white rounded-xl p-8 shadow'>
            <div className="divide-y-2" >
                <h2 className='text-xl '>Detalle del Inmueble</h2>
                <div className="flex flex-col gap-5 mt-3">
                    <p><strong>Nombre:</strong> {inmueble.nombre}</p>
                    <p><strong>Dirección:</strong> {inmueble.direccion}</p>
                    <p><strong>Teléfono:</strong> {inmueble.telefono}</p>
                    <p><strong>Capacidad de Aforo:</strong> {inmueble.capacidadAforo}</p>
                </div>
                <div className="flex justify-end gap-4 py-5">
                    <Link
                        to={`/`}
                        className='bg-blue-700 text-white px-5 py-2 rounded hover:bg-blue-600'
                    >
                        Volver a la lista
                    </Link>
                    <Link
                        to={`/create/${inmueble.id}`}
                        className='bg-blue-700 text-white px-5 py-2 rounded hover:bg-blue-600'
                    >
                        Editar
                    </Link>
                </div>
            </div>

        </section>
    );
};

export default Details;
