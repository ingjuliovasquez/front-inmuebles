import { useRoutes } from 'react-router-dom'
import Index from '../pages/Index'
import IndexLayout from '../layouts/IndexLayout'
import Details from '../pages/Details'
import Create from '../pages/Create'

export default function Router() {
    return useRoutes([
        {
            path: "",
            element: <IndexLayout />,
            children: [
                { path: "", element: <Index /> },
                { path: "details/:id", element: <Details /> },
                { path: "create", element: <Create /> },
                { path: "create/:id", element: <Create /> },
            ]
        },
        {
            path: "*",
            element: <>No existe esta página</>
        }
    ])
}
